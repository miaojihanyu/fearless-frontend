function createCard(title, description, pictureUrl,starts,ends,locations){
    return `
          <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${title}</h5>
              <h6 class="card-subtitle mb-2 text-muted">${locations}</h6>

              <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            <small class="text-muted">From ${starts} To:${ends}</small>

          </div>
        `
   
   }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
try {
    const response = await fetch(url);

    if (!response.ok) {

        } else {
      const data = await response.json();
      console.log(data)

      const conference = data.conferences[0];

      const detailUrl = `http://localhost:8000${conference.href}`;
      const detailResponse = await fetch(detailUrl);
      console.log(detailResponse)
      if (detailResponse.ok) {
        const details = await detailResponse.json();
        const title = details.conference.name;
        const description = details.conference.description;
        const pictureUrl = details.conference.location.picture_url;
        const locations =details.conference.location.name
        const starts = new Date(details.conference.starts).toDateString();
        const ends = new Date(details.conference.ends).toDateString();
        const html=createCard(title, description, pictureUrl,starts,ends,locations)
        console.log(html)
        const column = document.querySelector('.col');
        column.innerHTML += html;
      }

    }
  } catch (e) {
    console.log(e)
//    console.error('An error occurred:', error.message)
  }

});

